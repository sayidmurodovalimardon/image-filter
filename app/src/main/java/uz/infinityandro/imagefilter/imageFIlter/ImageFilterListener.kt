package uz.infinityandro.imagefilter.imageFIlter

import uz.infinityandro.imagefilter.data.ImageFilter

interface ImageFilterListener {
    fun onFilterSelected(imageFilter: ImageFilter)
}