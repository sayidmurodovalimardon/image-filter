package uz.infinityandro.imagefilter.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.imagefilter.data.ImageFilter
import uz.infinityandro.imagefilter.databinding.ItemContainerFilterBinding
import uz.infinityandro.imagefilter.imageFIlter.ImageFilterListener

class ImageFilterAdapter(var list:List<ImageFilter>,val listener:(imageFilter:ImageFilter)->Unit):RecyclerView.Adapter<ImageFilterAdapter.VH>() {

    inner class VH(var itemContainerFilterBinding: ItemContainerFilterBinding):RecyclerView.ViewHolder(itemContainerFilterBinding.root){
        fun onBind(imageFilter: ImageFilter) = with(itemContainerFilterBinding){
            itemContainerFilterBinding.imageFilter.setImageBitmap(imageFilter.filterPreview)
            itemContainerFilterBinding.text.text=imageFilter.name
            itemContainerFilterBinding.root.setOnClickListener {
                listener(imageFilter)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemContainerFilterBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])

    }

    override fun getItemCount(): Int =list.size
}