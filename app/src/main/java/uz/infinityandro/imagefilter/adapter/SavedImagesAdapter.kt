package uz.infinityandro.imagefilter.adapter

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.infinityandro.imagefilter.databinding.ItemSavedImagesBinding
import java.io.File

class SavedImagesAdapter(
    var list: List<Pair<File, Bitmap>>,
    val listener: (file:File) -> Unit
) : RecyclerView.Adapter<SavedImagesAdapter.VH>() {
    inner class VH(var itemSavedImagesBinding: ItemSavedImagesBinding) :
        RecyclerView.ViewHolder(itemSavedImagesBinding.root) {
        fun onBind(pair: Pair<File, Bitmap>) {
            itemSavedImagesBinding.imagesSaved.setImageBitmap(pair.second)
            itemSavedImagesBinding.imagesSaved.setOnClickListener {
                listener(pair.first)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(
            ItemSavedImagesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.onBind(list[position])
    }

    override fun getItemCount(): Int = list.size
}