package uz.infinityandro.imagefilter.viewmodel

import android.graphics.Bitmap
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import uz.infinityandro.imagefilter.data.ImageFilter
import uz.infinityandro.imagefilter.repository.EditImageRepository
import uz.infinityandro.imagefilter.repository.EditImageRepositoryImpl
import uz.infinityandro.imagefilter.utility.Coroutine

class EditImageViewModel(private val editImageRepository: EditImageRepository) : ViewModel() {
    //region:: Prepare image preview
    var imagePreviewDataSet = MutableLiveData<ImagePreviewDataState>()
    val uiState: LiveData<ImagePreviewDataState> get() = imagePreviewDataSet

    fun prepareImagePreview(imageUri: Uri) {
        Coroutine.io {
            kotlin.runCatching {
                emitUiState(isLoading = true)
                editImageRepository.prepareImagePreview(imageUri)
            }.onSuccess {
                if (it != null) {
                    emitUiState(bitmap = it)
                } else {
                    emitUiState(error = "Unable to prepare image preview")
                }

            }.onFailure {
                emitUiState(error = it.message.toString())
            }
        }
    }

    private fun emitUiState(
        isLoading: Boolean = false,
        bitmap: Bitmap? = null,
        error: String? = null
    ) {
        var dateState = ImagePreviewDataState(isLoading, bitmap, error)
        imagePreviewDataSet.postValue(dateState)
    }

    data class ImagePreviewDataState(
        val isLoading: Boolean,
        val bitmap: Bitmap?,
        val error: String?
    )
//endregion

    //region:: Load Image Filters
    private val imageFilterDataState = MutableLiveData<ImageFiltersDataState>()
    val imageFiltersUiState: LiveData<ImageFiltersDataState> get() = imageFilterDataState

    fun loadImageFilters(orginalImage: Bitmap) {
        Coroutine.io {
            kotlin.runCatching {
                emitImageFiltersUiState(isLoading = true)
                editImageRepository.getImageFilter(orginalImage)
            }.onSuccess {
                emitImageFiltersUiState(imageFilter = it)
            }.onFailure {
                emitImageFiltersUiState(error = it.localizedMessage)
            }
        }
    }

    private fun getPreviewImage(originalImage: Bitmap): Bitmap {
        return kotlin.runCatching {
            val previewWidth = 150
            val previewHeith = originalImage.height * previewWidth / originalImage.width
            Bitmap.createScaledBitmap(originalImage, previewWidth, previewHeith, false)
        }.getOrDefault(originalImage)
    }

    private fun emitImageFiltersUiState(
        isLoading: Boolean = false,
        imageFilter: List<ImageFilter>? = null,
        error: String? = null
    ) {
        val dataState = ImageFiltersDataState(isLoading, imageFilter, error)
        imageFilterDataState.postValue(dataState)
    }

    data class ImageFiltersDataState(
        val isLoading: Boolean,
        val imageFilter: List<ImageFilter>?,
        val error: String?
    )
    //endregion

    //region:: Save Filtered Images
    private val saveFilteredImagesDataState = MutableLiveData<SaveFilteredImages>()
    val saveFilteredImagesUiState: LiveData<SaveFilteredImages> get() = saveFilteredImagesDataState

    fun saveFilteredImageTo(filteredImage:Bitmap){
        Coroutine.io {
            kotlin.runCatching {
                emitSaveFilteredImagesUiState(isLoading1 = true)
                editImageRepository.saveImageFilter(filteredImage)
            }.onSuccess {saveImage->
                emitSaveFilteredImagesUiState(uri = saveImage)
            }.onFailure {
                emitSaveFilteredImagesUiState(error = it.message.toString())

            }
        }
    }
    private fun emitSaveFilteredImagesUiState(
        isLoading1: Boolean=false,
        uri: Uri? = null,
        error: String? = null
    ){
        val dataState=SaveFilteredImages(isLoading1, uri, error)
        saveFilteredImagesDataState.postValue(dataState)
    }

    data class SaveFilteredImages(
        var isLoading: Boolean,
        var uri: Uri?,
        var error: String?
    )
    //endregion
}
