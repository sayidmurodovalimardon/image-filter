package uz.infinityandro.imagefilter.viewmodel

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.infinityandro.imagefilter.repository.SavedImagesRepository
import uz.infinityandro.imagefilter.repository.SavedImagesRepositoryImpl
import uz.infinityandro.imagefilter.utility.Coroutine
import java.io.File

class SavedImagesViewModel(private val repositoryImpl: SavedImagesRepository) : ViewModel() {
    private val savedImagesDataState = MutableLiveData<SavedImagesDataState>()
    val savedImagesUiState: LiveData<SavedImagesDataState> get() = savedImagesDataState

    fun loadSavedImages() {
        Coroutine.io {
            kotlin.runCatching {
                emitSavedImagesUiState(isLoading = true)
                repositoryImpl.loadSavedImages()
            }.onSuccess {
                if (it.isNullOrEmpty()) {
                    emitSavedImagesUiState(error = "No Image Found")
                } else {
                    emitSavedImagesUiState(savedImagesList = it)
                }

            }.onFailure {
                emitSavedImagesUiState(error = it.message.toString())
            }
        }
    }

    private fun emitSavedImagesUiState(
        isLoading: Boolean = false,
        savedImagesList: List<Pair<File, Bitmap>>? = null,
        error: String? = null
    ) {
        val dataState = SavedImagesDataState(isLoading, savedImagesList, error)
        savedImagesDataState.postValue(dataState)

    }

    data class SavedImagesDataState(
        val isLoading: Boolean,
        val savedImagesList: List<Pair<File, Bitmap>>?,
        val error: String?
    )
}