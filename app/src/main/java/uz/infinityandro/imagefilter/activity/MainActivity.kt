package uz.infinityandro.imagefilter.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import uz.infinityandro.imagefilter.R
import uz.infinityandro.imagefilter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    companion object{
        private const val REQUEST_CODE_PICK_IMAGE=1
        const val KEY_IMAGE_URI="imageUri"
    }
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListener()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        window.statusBarColor=ContextCompat.getColor(this, R.color.colorPrimary)

    }

    private fun setListener() {
        binding.editImage.setOnClickListener {
            Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also { pickerIntent->
                pickerIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                startActivityForResult(pickerIntent, REQUEST_CODE_PICK_IMAGE)
            }

        }
        binding.saveButton.setOnClickListener {
            Intent(applicationContext,SaveImagesActivity::class.java).also {
                startActivity(it)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode== REQUEST_CODE_PICK_IMAGE && resultCode== RESULT_OK){
            data?.data.let {imageUri->
                Intent(this, EditImageActivity::class.java).also {
                    it.putExtra(KEY_IMAGE_URI,imageUri)
                    startActivity(it)
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}