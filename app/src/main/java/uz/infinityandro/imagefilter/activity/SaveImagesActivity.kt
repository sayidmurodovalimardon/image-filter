package uz.infinityandro.imagefilter.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.imagefilter.R
import uz.infinityandro.imagefilter.adapter.SavedImagesAdapter
import uz.infinityandro.imagefilter.databinding.ActivitySaveImagesBinding
import uz.infinityandro.imagefilter.utility.displayToast
import uz.infinityandro.imagefilter.viewmodel.SavedImagesViewModel

class SaveImagesActivity : AppCompatActivity() {
    companion object{
        const val KEY_FILTERED_IMAGE_URI="filteredImageUri"
    }
    private lateinit var binding: ActivitySaveImagesBinding
    private val viewModel: SavedImagesViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySaveImagesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setListener()
        setUpObservers()
        viewModel.loadSavedImages()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    private fun setUpObservers() {
        viewModel.savedImagesUiState.observe(this, {
            val savedImages = it ?: return@observe
            binding.savedImagesProgress.visibility =
                if (savedImages.isLoading) View.VISIBLE else View.GONE
            savedImages.savedImagesList?.let {
                var adapter=SavedImagesAdapter(it){file->
                val fileUri=FileProvider.getUriForFile(applicationContext,"${packageName}.provider",file)
                    Intent(this,FilteredImageActivity::class.java).also {filter->
                        filter.putExtra(KEY_FILTERED_IMAGE_URI,fileUri)
                        startActivity(filter)
                    }

                }
                binding.saveRecycler.adapter=adapter
            } ?: kotlin.run {
                savedImages.error?.let {
                    displayToast(it)
                }
            }
        })
    }

    private fun setListener() {
        binding.back.setOnClickListener {
            onBackPressed()
        }
    }
}
