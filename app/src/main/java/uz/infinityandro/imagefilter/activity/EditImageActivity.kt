package uz.infinityandro.imagefilter.activity

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.MutableLiveData
import jp.co.cyberagent.android.gpuimage.GPUImage
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.infinityandro.imagefilter.adapter.ImageFilterAdapter
import uz.infinityandro.imagefilter.data.ImageFilter
import uz.infinityandro.imagefilter.databinding.ActivityEditImageBinding
import uz.infinityandro.imagefilter.imageFIlter.ImageFilterListener
import uz.infinityandro.imagefilter.utility.displayToast
import uz.infinityandro.imagefilter.utility.show
import uz.infinityandro.imagefilter.viewmodel.EditImageViewModel

class EditImageActivity : AppCompatActivity() {
    companion object {
        const val KEY_FILTERED_IMAGE_URI = "filteredImageUri"
    }

    private lateinit var binding: ActivityEditImageBinding
    private val viewModelModule: EditImageViewModel by viewModel()
    private lateinit var pguImage: GPUImage
    private lateinit var orginalBitmap: Bitmap
    private val filteredBitmap = MutableLiveData<Bitmap>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setListener()
        prepareImagePreview()
        setUpObservers()
        setFilterImage()

    }

    private fun setFilterImage() {
        viewModelModule.imageFiltersUiState.observe(this, {
            val imageFilter = it ?: return@observe
            binding.imageFilterProgressBarr.visibility =
                if (imageFilter.isLoading) View.VISIBLE else View.GONE
            imageFilter.imageFilter?.let {

                ImageFilterAdapter(it) {
                    with(it) {
                        with(pguImage) {
                            setFilter(filter)
                            filteredBitmap.value = bitmapWithFilterApplied
                        }
                    }
                }.also {
                    binding.recycler.adapter = it
                }
            } ?: kotlin.run {
                imageFilter.error?.let {
                    displayToast(it)
                }
            }

        })
        filteredBitmap.observe(this, { bit ->
            binding.imagePreview.setImageBitmap(bit)
        })

        viewModelModule.saveFilteredImagesUiState.observe(this, {
            val saveFilteredImagesData = it ?: return@observe
            if (saveFilteredImagesData.isLoading) {
                binding.done.visibility = View.GONE
                binding.savingProgressBar.visibility = View.VISIBLE
            } else {
                binding.done.visibility = View.VISIBLE
                binding.savingProgressBar.visibility = View.GONE
            }
            saveFilteredImagesData.uri?.let { uri ->
                Intent(this, FilteredImageActivity::class.java).also {
                    it.putExtra(KEY_FILTERED_IMAGE_URI, uri)
                    startActivity(it)
                }
            } ?: kotlin.run {
                saveFilteredImagesData.error?.let {
                    displayToast(it)
                }
            }


        })
    }

    private fun setUpObservers() {

        viewModelModule.uiState.observe(this, {
            val dataState = it ?: return@observe
            binding.progressBar.visibility = if (dataState.isLoading) View.VISIBLE else View.GONE
            dataState.bitmap?.let {
                orginalBitmap = it
                filteredBitmap.postValue(it)
                with(orginalBitmap) {
                    pguImage.setImage(this)
                    binding.imagePreview.show()
                    viewModelModule.loadImageFilters(this)
                }


            }
        })
    }

    private fun prepareImagePreview() {
        pguImage = GPUImage(applicationContext)
        intent.getParcelableExtra<Uri>(MainActivity.KEY_IMAGE_URI)?.let {
            viewModelModule.prepareImagePreview(it)
        }
    }

//    private fun displayImageView() {
//        intent.getParcelableExtra<Uri>(MainActivity.KEY_IMAGE_URI)?.let {
//            val inputStream = contentResolver.openInputStream(it)
//            val bitmap = BitmapFactory.decodeStream(inputStream)
//            binding.imagePreview.setImageBitmap(bitmap)
//            binding.progressBar.visibility=View.GONE
//        }
//    }

    private fun setListener() {
        binding.back.setOnClickListener {
            onBackPressed()
        }
        binding.done.setOnClickListener {
            filteredBitmap.value?.let {
                viewModelModule.saveFilteredImageTo(it)
            }
        }
        binding.imagePreview.setOnLongClickListener {
            binding.imagePreview.setImageBitmap(orginalBitmap)
            return@setOnLongClickListener false
        }
        binding.imagePreview.setOnClickListener {
            binding.imagePreview.setImageBitmap(filteredBitmap.value)
        }
    }


}