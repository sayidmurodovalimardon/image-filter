package uz.infinityandro.imagefilter.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import uz.infinityandro.imagefilter.R
import uz.infinityandro.imagefilter.databinding.ActivityFilteredImageBinding
import uz.infinityandro.imagefilter.databinding.ItemContainerFilterBinding

class FilteredImageActivity : AppCompatActivity() {
    private lateinit var uri: Uri
    private lateinit var binding: ActivityFilteredImageBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityFilteredImageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        displayFilteredImages()
        setListener()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

    }

    private fun displayFilteredImages() {
        intent.getParcelableExtra<Uri>(EditImageActivity.KEY_FILTERED_IMAGE_URI)?.let {
            uri=it
            binding.imageFilter.setImageURI(it)
        }
    }
    private fun setListener(){
        binding.share.setOnClickListener {
            with(Intent(Intent.ACTION_SEND)){
                putExtra(Intent.EXTRA_STREAM,uri)
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                type="image/*"
                startActivity(this)
            }
        }
        binding.back.setOnClickListener {
            onBackPressed()
        }
    }
}