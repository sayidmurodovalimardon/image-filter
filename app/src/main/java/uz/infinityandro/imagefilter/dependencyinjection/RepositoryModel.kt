package uz.infinityandro.imagefilter.dependencyinjection

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import uz.infinityandro.imagefilter.repository.EditImageRepository
import uz.infinityandro.imagefilter.repository.EditImageRepositoryImpl
import uz.infinityandro.imagefilter.repository.SavedImagesRepository
import uz.infinityandro.imagefilter.repository.SavedImagesRepositoryImpl

val RepositoryModel = module {
    factory<EditImageRepository> { EditImageRepositoryImpl(androidContext()) }
    factory <SavedImagesRepository> { SavedImagesRepositoryImpl(androidContext())}
}