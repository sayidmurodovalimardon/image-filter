package uz.infinityandro.imagefilter.dependencyinjection

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.infinityandro.imagefilter.viewmodel.EditImageViewModel
import uz.infinityandro.imagefilter.viewmodel.SavedImagesViewModel

val viewModelModul= module {
    viewModel { EditImageViewModel(editImageRepository = get()) }
    viewModel { SavedImagesViewModel(repositoryImpl = get() ) }
}