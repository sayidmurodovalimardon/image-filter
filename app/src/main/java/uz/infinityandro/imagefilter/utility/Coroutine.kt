package uz.infinityandro.imagefilter.utility

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object Coroutine {
    fun io(work:suspend (()->Unit))=
        CoroutineScope(Dispatchers.IO).launch { work() }
}