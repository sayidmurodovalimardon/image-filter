package uz.infinityandro.imagefilter.utility

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import uz.infinityandro.imagefilter.dependencyinjection.RepositoryModel
import uz.infinityandro.imagefilter.dependencyinjection.viewModelModul

@Suppress("unused")
class AppConfig:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppConfig)
            modules(listOf(RepositoryModel, viewModelModul))
        }
    }
}