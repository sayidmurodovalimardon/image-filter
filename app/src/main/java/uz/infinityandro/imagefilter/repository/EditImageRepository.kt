package uz.infinityandro.imagefilter.repository

import android.graphics.Bitmap
import android.net.Uri
import uz.infinityandro.imagefilter.data.ImageFilter

interface EditImageRepository {
    suspend fun prepareImagePreview(imageUri:Uri):Bitmap?
    suspend fun getImageFilter(image:Bitmap):List<ImageFilter>
    suspend fun saveImageFilter(filteredImage:Bitmap):Uri?
}