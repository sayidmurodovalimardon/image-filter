package uz.infinityandro.imagefilter.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import java.io.File

class SavedImagesRepositoryImpl(private val context: Context) : SavedImagesRepository {
    override suspend fun loadSavedImages(): List<Pair<File, Bitmap>>? {
        var savedImages = ArrayList<Pair<File, Bitmap>>()
        val dir = File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Save Images"
        )
        dir.listFiles()?.let { data ->
            data.forEach { file ->
                savedImages.add(Pair(file, getPreviewBitmap(file)))

            }
            return savedImages
        } ?: return null

    }


    private fun getPreviewBitmap(file: File): Bitmap {
        val orginlaBItmap = BitmapFactory.decodeFile(file.absolutePath)
        val width = 150
        val height = ((orginlaBItmap.height * width) / orginlaBItmap.width)
        return Bitmap.createScaledBitmap(orginlaBItmap, width, height, false)
    }
}